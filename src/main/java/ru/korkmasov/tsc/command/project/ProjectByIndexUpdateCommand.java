package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.exeption.empty.EmptyNameException;
import ru.korkmasov.tsc.exeption.system.IndexIncorrectException;
import ru.korkmasov.tsc.util.TerminalUtil;

import static ru.korkmasov.tsc.util.ValidationUtil.checkIndex;
import static ru.korkmasov.tsc.util.ValidationUtil.isEmpty;

public class ProjectByIndexUpdateCommand extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER INDEX:]");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!checkIndex(index, serviceLocator.getProjectService().size(userId))) throw new IndexIncorrectException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        serviceLocator.getProjectService().updateProjectByIndex(userId, index, name, TerminalUtil.nextLine());
    }

}
