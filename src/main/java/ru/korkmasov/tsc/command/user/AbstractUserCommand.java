package ru.korkmasov.tsc.command.user;

import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.exeption.entity.UserNotFoundException;
import ru.korkmasov.tsc.model.User;

abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
