package ru.korkmasov.tsc.exeption.user;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email is already in use...");
    }

}
