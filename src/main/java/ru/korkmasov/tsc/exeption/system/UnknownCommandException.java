package ru.korkmasov.tsc.exeption.system;

import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.exeption.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Incorrect command ``" + command + "``. Use " + TerminalConst.CMD_HELP + ", it display list of terminal commands.");
    }

}
