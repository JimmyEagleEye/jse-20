package ru.korkmasov.tsc.exeption.empty;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty...");
    }

}
