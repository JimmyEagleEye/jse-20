package ru.korkmasov.tsc.exeption.empty;

import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyIndexException extends AbstractException {

    public EmptyIndexException() {
        super("Error. Index is empty");
    }

}

