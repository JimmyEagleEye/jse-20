package ru.korkmasov.tsc.exeption.empty;


import ru.korkmasov.tsc.exeption.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error. Id is empty");
    }

    public EmptyIdException(String value) {
        super("Error. " + value + " id is empty");
    }

}
