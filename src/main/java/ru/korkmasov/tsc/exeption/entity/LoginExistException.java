package ru.korkmasov.tsc.exeption.entity;

import ru.korkmasov.tsc.exeption.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException(String value) {
        super("Error. Login '" + value + "' already exist.");
    }

}
