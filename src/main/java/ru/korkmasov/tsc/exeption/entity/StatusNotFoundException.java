package ru.korkmasov.tsc.exeption.entity;

import ru.korkmasov.tsc.exeption.AbstractException;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error. Task not found.");
    }

}
