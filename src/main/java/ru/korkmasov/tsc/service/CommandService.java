package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.ICommandRepository;
import ru.korkmasov.tsc.api.service.ICommandService;
import ru.korkmasov.tsc.command.AbstractCommand;
import ru.korkmasov.tsc.util.ValidationUtil;

import java.util.Collection;


public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        if (ValidationUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        if (ValidationUtil.isEmpty(arg)) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getCommandNames();
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
