package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.api.service.IAuthService;
import ru.korkmasov.tsc.api.service.ITaskService;
import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exeption.empty.EmptyIdException;
import ru.korkmasov.tsc.exeption.empty.EmptyIndexException;
import ru.korkmasov.tsc.exeption.empty.EmptyNameException;
import ru.korkmasov.tsc.exeption.system.IndexIncorrectException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.ValidationUtil;

import java.util.Date;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository, IAuthService authService) {
        super(taskRepository, authService);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final Task task) {
        if (task == null) return null;
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task removeOneByName(String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task removeTaskByIndex(String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task findOneByIndex(String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > taskRepository.size(userId)) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task updateTaskByIndex(String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(String userId, final String name, final String nameNew, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyIndexException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task finishTaskById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task startTaskById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusById(String userId, String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String userId, String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(String userId, Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public String getIdByIndex(String userId, Integer index) {
        if (!ValidationUtil.checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        return taskRepository.getIdByIndex(index);
    }

    public boolean existsByName(String userId, String name) {
        if (ValidationUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.existsByName(userId, name);
    }

}
