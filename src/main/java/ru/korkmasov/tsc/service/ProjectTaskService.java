package ru.korkmasov.tsc.service;

import ru.korkmasov.tsc.api.repository.IProjectRepository;
import ru.korkmasov.tsc.api.service.IProjectTaskService;
import ru.korkmasov.tsc.api.repository.ITaskRepository;
import ru.korkmasov.tsc.exeption.empty.EmptyIdException;
import ru.korkmasov.tsc.exeption.empty.EmptyNameException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.ValidationUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findALLTaskByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findALLTaskByProjectId(userId, projectId);
    }

    @Override
    public Task assignTaskByProjectId(final String userId, final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.assignTaskByProjectId(userId, projectId, taskId);
    }

    @Override
    public Task unassignTaskByProjectId(final String userId, final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unassignTaskByProjectId(userId, taskId);
    }

    @Override
    public List<Task> removeTasksByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        if (ValidationUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
        return null;
    }

    @Override
    public void removeProjectByName(final String userId, final String projectName) {
        if (ValidationUtil.isEmpty(projectName)) throw new EmptyNameException();
        String projectId = projectRepository.getIdByName(userId, projectName);
        if (ValidationUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeOneByName(userId, projectName);
    }


}
