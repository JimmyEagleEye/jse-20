package ru.korkmasov.tsc.util;

import ru.korkmasov.tsc.enumerated.Sort;
import ru.korkmasov.tsc.enumerated.Status;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final int index, int size) {
        if (index < 0) return false;
        return index <= size - 1;
    }

    static boolean checkStatus(final String status) {
        for (Status s : Status.values()) {
            if (s.name().equals(status)) {
                return true;
            }
        }
        return false;
    }

    static boolean checkSort(final String sortOrder) {
        for (Sort s : Sort.values()) {
            if (s.name().equals(sortOrder)) {
                return true;
            }
        }
        return false;
    }


}
