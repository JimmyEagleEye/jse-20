package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(E entity);

    //void clear(String userId);

    //List<E> findAll(String userId, Comparator<E> comparator);

    //List<E> findAll(String userId);

    E findById(String id);

    void remove(E entity);

    E removeById(String id);

    int size();

}
