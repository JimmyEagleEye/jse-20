package ru.korkmasov.tsc.api.service;

public interface ILoggerService {

    void info(String message);

    void command(String message);

    void error(Exception e);
}
