package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.model.User;

public interface IAuthService {
    String getUserId();

    User getUser();

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);
}
